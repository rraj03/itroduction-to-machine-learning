import { Component } from '@angular/core';
import {Router} from "@angular/router";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'introduction-to-machine-learning';
  constructor(public router: Router) { }
 course(){
  this.router.navigate(['chapters']);
 } 
}
