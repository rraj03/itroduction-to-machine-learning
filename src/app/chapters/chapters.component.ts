import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chapters',
  templateUrl: './chapters.component.html',
  styleUrls: ['./chapters.component.css']
})
export class ChaptersComponent implements OnInit {
  
  topic:boolean=false;
  constructor() { }

  ngOnInit() {
  }
  openTopics(){
    if(this.topic)
        this.topic=false;
    else
        this.topic=true;
  }
}
