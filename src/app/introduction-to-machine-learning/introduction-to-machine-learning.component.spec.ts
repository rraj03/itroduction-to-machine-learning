import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroductionToMachineLearningComponent } from './introduction-to-machine-learning.component';

describe('IntroductionToMachineLearningComponent', () => {
  let component: IntroductionToMachineLearningComponent;
  let fixture: ComponentFixture<IntroductionToMachineLearningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductionToMachineLearningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroductionToMachineLearningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
