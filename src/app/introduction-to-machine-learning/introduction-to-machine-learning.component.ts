import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-introduction-to-machine-learning',
  templateUrl: './introduction-to-machine-learning.component.html',
  styleUrls: ['./introduction-to-machine-learning.component.css']
})
export class IntroductionToMachineLearningComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }
  startCourse(){
    this.router.navigate(['chapters']);
    console.log('1')
  }
}
